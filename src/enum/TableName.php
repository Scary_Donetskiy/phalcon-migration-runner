<?php
/**
 * Created by PhpStorm.
 * User: vladyslavpozdnyakov
 * Date: 17.03.2018
 * Time: 14:50
 */

namespace Vados\MigrationRunner\enum;

/**
 * Class TableName
 * @package Vados\MigrationRunner\enum
 */
abstract class TableName
{
    const TBL_MIGRATION = 'tbl_migration';
}